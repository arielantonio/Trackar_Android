package com.laasu.gpstracker.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Message;


/**
 * Created by ENVY on 15-05-2016.
 */
public class ClassTK103 {

    //msg stop+password
    public static String StopEngine(String password){
        return "stop"+password;
    }
    //msg resume+password
    public static String ResumenEngine(String password){
        return "resume"+password;
    }
    //msg fix030s005n+password
    public static String Location(String password, String seconds, String times){
        return "fix"+AddZero(seconds)+"s"+AddZero(times)+"n"+password;
    }



    //Reciever "Stop engine Succeed"
    public static String RStopEngine(){ return "Stop engine Succeed"; }

    public static ClassLocation ReadLocation(String Msg)
    {
        String[] Message = Msg.split("\n");
        //get Latitud

        ClassLocation ClLocation= new ClassLocation();
        ClLocation.setLatitud(StrValue("lat:",Message));
        ClLocation.setLongitud(StrValue("long:",Message));
        ClLocation.setSpeed(StrValue("speed:",Message));
        ClLocation.setTime(StrValue("T:",Message));
        ClLocation.setUrl(StrValue("http:",Message));
        ClLocation.setPower(StrValue("Pwr:",Message));
        ClLocation.setDoor(StrValue("Door:",Message));
        ClLocation.setAcc(StrValue("ACC:",Message));
        ClLocation.setLac(StrValue("Lac:",Message));
        ClLocation.setLast(StrValue("Last:",Message));
        GetPositionInURL(ClLocation);
        return ClLocation;
    }

    public static void GetPositionInURL(ClassLocation ClLocation)
    {
        String PatronIni ="q&q=", PatronFin ="&";
        String Lat="", Lon="";
        String[] LatLon;
        int ini =0, fin=0;
        if (ClLocation.getLongitud()=="0" || ClLocation.getLatitud()=="0") {
            ini = ClLocation.getUrl().indexOf(PatronIni) + PatronIni.length();
            fin = ClLocation.getUrl().lastIndexOf(PatronFin);
            LatLon = ClLocation.getUrl().substring(ini, fin).split(",");
            ClLocation.setLatitud(LatLon[0]);
            ClLocation.setLongitud(LatLon[1]);
        }
    }

    public static String StrValue(String Str1, String[] Msg){
        int Position1=0;
        String TempStatus;
        String[] Status;

        try {
            for (int count=0;count<Msg.length;count++)
            {
                if(Msg[count].indexOf(Str1)!=-1){
                    Position1 = Msg[count].indexOf(Str1);
                    Status = Msg[count].split(" ");
                    if (Status.length>1){
                        TempStatus =getStatus(Status,Str1);
                        if (!TempStatus.equals(""))
                            return TempStatus;
                    }
                    if(Str1.equals("http:"))
                    { return Msg[count]; }
                    else{ return Msg[count].substring(Position1 + Str1.length(),Msg[count].length()); }
                }
            }
        }catch(Exception e){
            return "";
        }
        return "0";
    }

    public static String AddZero(String num){
        if(num.length()<3){
            if(num.length()==1){ return "00"+num;}
            else{ return "0"+num; }
        }
        return num;
    }

    public static String getStatus(String[] Status, String Str1){
        try {
            for (int count=0;count<Status.length;count++){
                if(Status[count].equals(Str1)) {
                    return Status[count+1];
            }
        }
        }catch(Exception e){
            return "";
        }
        return "";
    }

}
