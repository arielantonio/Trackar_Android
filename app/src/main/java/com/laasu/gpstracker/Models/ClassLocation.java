package com.laasu.gpstracker.Models;

/**
 * Created by ENVY on 03-07-2016.
 *
 * Clase que almacena todos el resultado del SMS
 */
public class ClassLocation {

    private String Latitud="";
    private String Longitud="";
    private String Speed="";
    private String Time="";
    private String Url="";
    private String Power="";
    private String Door="";
    private String Acc="";
    private String Lac="";
    private String Last="";

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getPower() {
        return Power;
    }

    public void setPower(String power) {
        Power = power;
    }

    public String getDoor() {
        return Door;
    }

    public void setDoor(String door) {
        Door = door;
    }

    public String getAcc() {
        return Acc;
    }

    public void setAcc(String acc) {
        Acc = acc;
    }

    public String getLac() { return Lac; }

    public void setLac(String lac) { Lac = lac; }

    public String getLast() { return Last; }

    public void setLast(String last) { Last = last; }
}
