package com.laasu.gpstracker.Models;


import java.io.Serializable;

/**
 * Created by ENVY on 16-05-2016.
 */
public class ClassParamModel implements Serializable {

    private String password="";
    private String Movil="";
    private String LocationSec="";
    private String LocationMin="";
    private String LocationHour="";
    private String LocationNumRepeat="";
    private String Action="";
    private String Model="";
    private String MSG ="";
    private String FileConfig="";
    private ClassLocation ClLocation=null;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMovil() {
        return Movil;
    }

    public void setMovil(String movil) {
        Movil = movil;
    }

    public String getLocationSec() {
        return LocationSec;
    }

    public void setLocationSec(String locationSec) {
        LocationSec = locationSec;
    }

    public String getLocationMin() {
        return LocationMin;
    }

    public void setLocationMin(String locationMin) {
        LocationMin = locationMin;
    }

    public String getLocationHour() {
        return LocationHour;
    }

    public void setLocationHour(String locationHour) {
        LocationHour = locationHour;
    }

    public String getLocationNumRepeat() {
        return LocationNumRepeat;
    }

    public void setLocationNumRepeat(String locationNumRepeat) {
        LocationNumRepeat = locationNumRepeat;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getMSG() {
        return MSG;
    }

    public void setMSG(String MSG) {
        this.MSG = MSG;
    }


    public String getFileConfig() {
        return FileConfig;
    }

    public void setFileConfig(String fileConfig) {
        FileConfig = fileConfig;
    }

    public ClassLocation getClLocation() {
        return ClLocation;
    }

    public void setClLocation(ClassLocation clLocation) {
        ClLocation = clLocation;
    }
}
