package com.laasu.gpstracker;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.laasu.gpstracker.Models.ClassModels;
import com.laasu.gpstracker.Models.ClassParamModel;

public class EngineActivity extends Activity {

    private BroadcastReceiver mIntentReceiver;
    private String Movil;
    private String Pass;
    private ClassParamModel ParamModel;
    TextView lbl_Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_engine);
        Intent i = getIntent();
        ParamModel = (ClassParamModel) i.getExtras().getSerializable("ParamModel");
        setContentView(R.layout.activity_engine);
        lbl_Result = (TextView) findViewById(R.id.lbl_result);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        mIntentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mov = intent.getStringExtra("movil");
                String msg = intent.getStringExtra("msg");
                if (mov.equals(ParamModel.getMovil())){
                    lbl_Result.setText(msg);
                    //showmsg(mov+" : "+msg);
                }
            }
        };
        this.registerReceiver(mIntentReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mIntentReceiver);
    }

    public void showmsg(String valor){
        Toast toast = Toast.makeText(this, valor, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    public void SendSMSOnEngine(View v){
        Toast.makeText(EngineActivity.this, getString(R.string.MsgSendSMSOnEngine),Toast.LENGTH_SHORT).show();
        try {
            ClassModels Models = new ClassModels();
            ParamModel.setAction(getString(R.string.Action_ResumenEngine));
            Models.ActionModels(this, ParamModel);
        }
        catch(Exception e){
            Toast.makeText(EngineActivity.this, getString(R.string.MsgSendSMSOnEngine),Toast.LENGTH_SHORT).show();
        }
    }

    public void SendSMSOffEngine(View v){
        Toast.makeText(EngineActivity.this, getString(R.string.MsgSendSMSOffEngine),Toast.LENGTH_SHORT).show();
        try {
            ClassModels Models = new ClassModels();
            ParamModel.setAction(getString(R.string.Action_StopEngine));
            Models.ActionModels(this, ParamModel);
        }
        catch(Exception e){
            Toast.makeText(EngineActivity.this, getString(R.string.MsgSendSMSOnEngine),Toast.LENGTH_SHORT).show();
        }
    }

    public void Exit(View v)
    {
        this.finish();
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }
}
