package com.laasu.gpstracker.SharePreFunctions;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ENVY on 14-05-2016.
 */
public class ClassSharePreference {

    SharedPreferences prefs;
    String NameFile;

    public ClassSharePreference(SharedPreferences prefs) {
        this.prefs = prefs;
    }


    public boolean Save(String FieldName, String FieldText ){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FieldName, FieldText);
        editor.commit();
        return true;
    }

    public String getData(String FieldName){
        return prefs.getString(FieldName,"");
    }

}
