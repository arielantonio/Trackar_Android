package com.laasu.gpstracker;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.laasu.gpstracker.SharePreFunctions.ClassSharePreference;

public class ConfigActivity extends AppCompatActivity {

    private EditText num, pass, rpass, code;
    public Spinner SPModel;

    public ClassSharePreference ClassSP;

    public String ConfigFile ;
    public String ConfigNum;
    public String ConfigPass;
    public String ConfigModel;
    public String ConfigCodeCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_config);
        setContentView(R.layout.activity_config);



        num = (EditText) findViewById(R.id.edt_num);
        pass = (EditText) findViewById(R.id.edt_pass);
        rpass = (EditText) findViewById(R.id.edt_rpass);
        code = (EditText) findViewById(R.id.edt_company);
        SPModel = (Spinner) findViewById(R.id.spn_model);

        LoadConfig();
        LoadSpinner();
        ClassSP = new ClassSharePreference(getSharedPreferences(ConfigFile, Context.MODE_PRIVATE));

        num.setText(ClassSP.getData(ConfigNum));
        pass.setText(ClassSP.getData(ConfigPass));
        rpass.setText(ClassSP.getData(ConfigPass));
        code.setText(ClassSP.getData(ConfigCodeCompany));

        ArrayAdapter myAdap = (ArrayAdapter) SPModel.getAdapter();
        int spinnerPosition = myAdap.getPosition(ClassSP.getData(ConfigModel));
        SPModel.setSelection(spinnerPosition);
    }

    // Almacena en un archivo la configuracion telefono y contrasena
    public void save(View v)
    {
        String anwser = IsValid();

        if (anwser.equals(getResources().getString(R.string.Success))){
            ClassSP.Save(ConfigNum,num.getText().toString());
            ClassSP.Save(ConfigPass,pass.getText().toString());
            ClassSP.Save(ConfigModel,SPModel.getSelectedItem().toString());
            ClassSP.Save(ConfigCodeCompany,code.getText().toString());
            this.finish();
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
        else{
            Toast toast = Toast.makeText(this, anwser, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }

    }

    public void Exit(View v)
    {
        this.finish();
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    //Valida si es vacio los campos ingresados
    public String IsValid(){
        if (num.getText().toString().equals(""))
        { return getResources().getString(R.string.ErrorMovilEmpty); }
        else if (pass.getText().toString().equals(""))
        { return getResources().getString(R.string.ErrorPassEmpty); }
        else if (!pass.getText().toString().equals(rpass.getText().toString()))
        { return getResources().getString(R.string.ErrorPassNoRepass); }
        else {
            return getResources().getString(R.string.Success);
        }
    }

    private void LoadConfig(){
        ConfigFile =getResources().getString(R.string.NameFileConfig);
        ConfigNum =getResources().getString(R.string.FieldNum);
        ConfigPass =getResources().getString(R.string.FieldPass);
        ConfigModel=getResources().getString(R.string.FieldModel);
        ConfigCodeCompany=getResources().getString(R.string.FieldCompany);
    }

    private void LoadSpinner()
    {
        ArrayAdapter adapter= ArrayAdapter.createFromResource(
                this,R.array.models,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPModel.setAdapter(adapter);
    }


}
