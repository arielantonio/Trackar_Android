package com.laasu.gpstracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.*;
import android.widget.Toast;

import com.laasu.gpstracker.Constants.ClassComunicator;
import com.laasu.gpstracker.Models.ClassModels;
import com.laasu.gpstracker.Models.ClassParamModel;
import com.laasu.gpstracker.SharePreFunctions.ClassSharePreference;

public class MainActivity extends AppCompatActivity {

    public ClassParamModel ParamModel=null;
    public ClassSharePreference ClassSP=null;
    public ClassModels Models=null;

    public Button ButtonOnOff;
    public Button ButtonMap;

    public static String BROADCAST_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setTitle(R.string.title_main);

        ButtonOnOff = (Button) findViewById(R.id.btn_OnOffMotor);
        ButtonMap = (Button) findViewById(R.id.btn_Map);

        Models = new ClassModels();
        ParamModel=new ClassParamModel();
        ClassSP = new ClassSharePreference(getSharedPreferences(getResources().getString(R.string.NameFileConfig)
                , Context.MODE_PRIVATE));

        ParamModel.setMovil(ClassSP.getData(getResources().getString(R.string.FieldNum)));
        ParamModel.setPassword(ClassSP.getData(getResources().getString(R.string.FieldPass)));
        ParamModel.setModel(ClassSP.getData(getResources().getString(R.string.FieldModel)));

        ValidateParam();
    }


    public void SMSOnOffEngine(View v){
        try{
        //sendBroadcast();
        Intent i = new Intent(this, EngineActivity.class);
        i.putExtra("ParamModel", ParamModel);
        startActivity(i);
        }catch (Exception e){
            e.toString();
        }
    }


    public void Location(View v){
        //sendBroadcast();
        Intent i = new Intent(this, LocationActivity.class);
        i.putExtra("ParamModel", ParamModel);
        startActivity(i);
    }

    public void ValidateParam(){
        if (ParamModel.getMovil()=="" || ParamModel.getMovil()==null &&
            ParamModel.getPassword()=="" || ParamModel.getPassword()==null &&
            ParamModel.getModel()=="" || ParamModel.getModel()==null)
        {
            ButtonOnOff.setEnabled(false);
            ButtonMap.setEnabled(false);
            return;
        }
        ButtonOnOff.setEnabled(true);
        ButtonMap.setEnabled(true);
    }

    public void Exit(View v)
    {
        finish();
    }





    public void SelectedConfiguration(View v){
        Intent i = new Intent(this, ConfigActivity.class);
        startActivity(i);
    }

    public void sendBroadcast(){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION);
        sendBroadcast(broadcast);
    }






}
