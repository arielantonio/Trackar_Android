package com.laasu.gpstracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.laasu.gpstracker.SharePreFunctions.ClassSharePreference;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ariel on 12-10-2016.
 */
public class SplashScreenActivity extends Activity {
    private static final long SPLASH_SCREEN_DELAY = 3000;

    private ImageView Front;
    public ClassSharePreference ClassSP=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splash_screen);

        Front = (ImageView)findViewById(R.id.portalView);

        ChangeMainImage();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Start the next activity
                Intent mainIntent = new Intent().setClass(
                        SplashScreenActivity.this, InitialActivity.class);
                startActivity(mainIntent);
                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
            }
        };
        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }

    public void ChangeMainImage()
    {
        /*Para utilizar la imagen vectorizada es necesario cambiar para dispositivos antiguos
        cambiar el tipo de imagen
         */
        int mResId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Front.setBackgroundResource(NewImage());
        } else {
            Front.setBackgroundResource(NewImage());
        }
    }

    public int NewImage(){
        String NumCompany="";
        ClassSP= new ClassSharePreference(getSharedPreferences(getResources().getString(R.string.NameFileConfig)
                , Context.MODE_PRIVATE));
        NumCompany = ClassSP.getData(getResources().getString(R.string.FieldCompany));
        switch (NumCompany){
            case "123": return  R.drawable.ic_logo_example;
            default:
                return R.drawable.ic_logo_vectorizado_1900;
        }
    }
}
