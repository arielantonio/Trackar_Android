package com.laasu.gpstracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.laasu.gpstracker.Models.ClassModels;
import com.laasu.gpstracker.Models.ClassParamModel;

public class LocationActivity extends FragmentActivity  {

    //public MapView GoogleMap;
    GoogleMap googleMap;
    MapView mapView;
    private BroadcastReceiver mIntentReceiver;
    private ClassParamModel ParamModel;
    private ClassModels Models;
    private CountDownTimer cTimer = null;

    private boolean isPaused =false;
    private boolean isCanceled =false;

    float zoom = (float) 16.0;

    private Toast aToast;

    @Override
    protected void onResume(){
        super.onResume();
        mapView.onResume();

        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        mIntentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mov = intent.getStringExtra(getString(R.string.Movil_Num));
                String msg = intent.getStringExtra(getString(R.string.Movil_Msg));
                if (mov.equals(ParamModel.getMovil())){
                    ParamModel.setAction(getString(R.string.Action_AutoTrack_MSG));
                    ParamModel.setMSG(msg);
                    Models.ActionModels(context, ParamModel);
                    PointGPS(ParamModel.getClLocation().getLatitud(),
                            ParamModel.getClLocation().getLongitud());
                    LatLng latLng = new LatLng(Double.parseDouble(ParamModel.getClLocation().getLatitud()),
                            Double.parseDouble(ParamModel.getClLocation().getLongitud()));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                    cTimer.cancel();
                    Toast.makeText(LocationActivity.this, getString(R.string.MsgSendFoundLocation), Toast.LENGTH_SHORT).show();
                    //cTimer.onFinish();
                    //cTimer.

                    PauseApp(2000);
                    SendSMS();
                    /*setMarker(new LatLng(Double.parseDouble(ParamModel.getClLocation().getLatitud()),
                            Double.parseDouble(ParamModel.getClLocation().getLongitud())),
                            "Tu vehiculo",
                            "Auto");*/
                }
                else
                {
                    Toast.makeText(LocationActivity.this, getString(R.string.ErrorFormato), Toast.LENGTH_SHORT).show();
                    //SendSMS();
                }
            }
        };
        this.registerReceiver(mIntentReceiver, intentFilter);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
        cTimer.cancel();
    }

    @Override
    protected void onPause(){
        super.onPause();
        mapView.onPause();
        cTimer.cancel();
        this.unregisterReceiver(this.mIntentReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setTitle(R.string.title_location);

        Intent i = getIntent();
        ParamModel = (ClassParamModel) i.getExtras().getSerializable("ParamModel");

        SendSMS();
        Toast.makeText(LocationActivity.this, getString(R.string.MsgSendSMS), Toast.LENGTH_SHORT).show();
        //Envia SMS para obtener ubicacion del Vehiculo
        /*Models = new ClassModels();
        ParamModel.setLocationHour("00");
        ParamModel.setLocationMin("00");
        ParamModel.setLocationNumRepeat("1");
        ParamModel.setLocationSec("10");
        ParamModel.setAction(getString(R.string.Action_AutoTrack));
        Models.ActionModels(this, ParamModel);*/
        /*************************************/
        
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        googleMap=mapView.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //googleMap.setMyLocationEnabled(true);

    }

    private void setMarker(LatLng position, String titulo, String info) {
        // Agregamos marcadores para indicar sitios de interéses.
        Marker myMaker = googleMap.addMarker(new MarkerOptions()
                .position(position)
                .title(titulo)  //Agrega un titulo al marcador
                .snippet(info)   //Agrega información detalle relacionada con el marcador
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))); //Color del marcador
    }

    private void SendSMS()
    {
        ThreadTimeOut();
        Models = new ClassModels();
        ParamModel.setLocationHour("00");
        ParamModel.setLocationMin("00");
        ParamModel.setLocationNumRepeat("1");
        ParamModel.setLocationSec("10");
        ParamModel.setAction(getString(R.string.Action_AutoTrack));
        Models.ActionModels(this, ParamModel);
    }

    private void ThreadTimeOut() {
        cTimer= new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                /*if(isPaused || isCanceled)
                {
                    //If user requested to pause or cancel the count down timer
                    cancel();
                }
                else {*/
                    //Toast.makeText(LocationActivity.this, "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
                    //showToast("seconds remaining: " + millisUntilFinished / 1000, 1000);
                    if ((millisUntilFinished / 1000)-1 <= 0) {
                        Toast.makeText(LocationActivity.this, getString(R.string.ErrorWithoutResponse), Toast.LENGTH_SHORT).show();
                    }
                //}
            }

            public void onFinish() {

                cancel();

                //PauseApp(2000);
                //SendSMS();
                //mTextField.setText("done!");
            }


        }.start();

    }

    private void PointGPS(String Lat, String Lon)
    {
        setMarker(new LatLng(Double.parseDouble(Lat),
                        Double.parseDouble(Lon)),
                "Tu vehiculo",
                "Auto");
    }

    private void PauseApp(int wait_time){
        try {
            Thread.sleep(wait_time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        cTimer.cancel();
        super.onBackPressed();
    }


    public void showToast(String Msg, int Duration) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = Duration;
        aToast = Toast.makeText(this, Msg, Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                aToast.show();
            }
            public void onFinish() {
                aToast.cancel();
            }
        };

        // Show the toast and starts the countdown
        aToast.show();
        toastCountDown.start();
    }






}
