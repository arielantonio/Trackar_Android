package com.laasu.gpstracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.database.Cursor;
import android.net.Uri;
import android.widget.TextView;



public class InitialActivity extends AppCompatActivity {

    public String ConfigFile;
    public String ConfigNum;
    public String ConfigPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LoadConfig();

        if (existConfig()){
            //setContentView(R.layout.activity_main);
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
        else{
            //setContentView(R.layout.activity_main);
            Intent i = new Intent(this, ConfigActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            //setContentView(R.layout.activity_config);
        }
        finish();
    }

    private void LoadConfig(){
        ConfigFile =getResources().getString(R.string.NameFileConfig);
        ConfigNum =getResources().getString(R.string.FieldNum);
        ConfigPass =getResources().getString(R.string.FieldPass);
    }

    private boolean existConfig(){
        String NumTel="";
        String Pass="";
        SharedPreferences prefs = getSharedPreferences(ConfigFile, Context.MODE_PRIVATE);
        NumTel = prefs.getString(ConfigNum,"");
        Pass = prefs.getString(ConfigPass,"");
        if (NumTel.equals("")){
            return false;
        }
        else if (Pass.equals("")){
            return false;
        }
        return true;
    }
}
