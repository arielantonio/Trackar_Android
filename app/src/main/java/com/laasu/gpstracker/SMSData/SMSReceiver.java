package com.laasu.gpstracker.SMSData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by ENVY on 16-05-2016.
 */
public class SMSReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        SmsMessage smsMessage;

        if (Build.VERSION.SDK_INT >= 19){
            SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            String format = intent.getStringExtra("format");

            SmsMessage sms = msgs[0];
            /*Log.v("TAG", "handleSmsReceived" + (sms.isReplace() ? "(replace)" : "") +
                    " messageUri: " +
                    ", address: " + sms.getOriginatingAddress() +
                    ", body: " + sms.getMessageBody());*/

            String message = sms.getMessageBody();
        }else{
            Bundle bundle= intent.getExtras();
            SmsMessage [] msgs = null;
            String str = "";
            if (bundle != null){
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i<msgs.length; i++){
                    Intent in = new Intent("SmsMessage.intent.MAIN");
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    in.putExtra("movil",msgs[i].getOriginatingAddress());
                    in.putExtra("msg",msgs[i].getMessageBody().toString());
                    context.sendBroadcast(in);
                }


            }
        }


    }
}
