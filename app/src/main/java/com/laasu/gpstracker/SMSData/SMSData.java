package com.laasu.gpstracker.SMSData;

/**
 * Created by ENVY on 21-03-2016.
 */
public class SMSData {
    private String number;

    private String body;

    public String getNumber(){
        return number;
    }

    public void setNumber(String number){
        this.number = number;
    }

    public String getBody(){
        return body;
    }

    public void setBody(String body){
        this.body = body;
    }
}
